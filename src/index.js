import { registerBlockType } from '@wordpress/blocks';

import './style.scss';

import Edit from './edit';
import save from './save';

registerBlockType('create-block/casino-rating', {
	/**
	 * @see ./edit.js
	 */
	edit: Edit,

	/**
	 * @see ./save.js
	 */
	save,
});

/**
 * Child blocks
 */
 import { metadata, name, settings } from './blocks/casino-item';

 registerBlockType({ name, ...metadata }, settings);