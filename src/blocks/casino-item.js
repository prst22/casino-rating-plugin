import { InnerBlocks, useBlockProps, InspectorControls, RichText } from "@wordpress/block-editor";
import { PanelBody, PanelRow, RangeControl, Icon } from "@wordpress/components";

import metadata from "./casino-items.json";

const { name } = metadata,
IMG_TEMPLATE = [
	["core/image", { 
		sizeSlug: 'small', 
		caption: false
	}]
],
Circle = (opt) => (
	<Icon dataRating={ opt.dataRating }
		icon={ (opt) => (
			<svg class="rating" width="80" height="80" viewport="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" data-rating={ opt.dataRating }>
				<circle class="rating__bg" r="33" cx="40" cy="40" fill="transparent" stroke-dasharray="207.35" stroke-dashoffset="0"></circle>
	            <circle class="rating__bar" r="33" cx="40" cy="40" fill="transparent" stroke-dasharray="207.35" stroke-dashoffset="207.35"></circle>
			</svg>
		) }
	/>
);

export { metadata, name };

export const settings = {
	icon: "sos",
	attributes: {
		casinoRating: {
			type: 'number',
			default: 5,
		},
		name: {
			type: 'string',
			default: '',
			selector: 'h2',
		},
		description: {
			type: 'string',
			default: '',
			selector: 'p'
		}
	},
    
	edit({attributes, setAttributes}) {
		const blockProps = useBlockProps( {
			className: 'casino',
		} );
		let initialPos = attributes.casinoRating || 5;

		return (
			<div {...blockProps}>
                <div class="casino__inner">
                    <InspectorControls style={{ marginBottom: "40px" }}>
                        <PanelBody>
                            <PanelRow>
                                <RangeControl
                                    label="Casino rating"
                                    initialPosition={ initialPos }
                                    min={ 0 }
                                    max={ 10 }
                                    step={ 0.1 }
                                    onChange={(value) => setAttributes({ casinoRating: value })}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    
                    <div class="casino_image">
                        <InnerBlocks template={ IMG_TEMPLATE } templateLock="all" />
                    </div> 

                    <div class="casino_info">
                        <RichText
                            tagName="h2"
                            placeholder="Casino name"
                            className="casino_name"
                            value={ attributes.name }
                            onChange={ (name) => setAttributes( { name } ) }
                        />
                        <div class="casino_ratiing_cnt">
                            <Circle dataRating={ attributes.casinoRating } />
                            <span class="counter">{ attributes.casinoRating }</span>
                        </div>
                    </div> 
                </div>
				<RichText
					tagName="p"
					placeholder="Casino description"
					className="casino__description"
					value={ attributes.description }
					onChange={ (description) => setAttributes( { description } ) }
				/>
			</div>
		);
	},
	save(props) {
		const blockProps = useBlockProps.save( {
			className: 'casino',
		} );
		return (
			<div {...blockProps}>
                <div class="casino__inner">
                    <div class="casino_image">
                        <InnerBlocks.Content/>
                    </div>  
                    <div class="casino_info">
                        <RichText.Content
                            tagName="h2"
                            className="casino_name"
                            value={ props.attributes.name }
                        />
                        <div class="casino_ratiing_cnt">
                            <Circle dataRating={ props.attributes.casinoRating } />
                            <span class="counter">{ props.attributes.casinoRating }</span>
                        </div> 
                    </div>
                </div>
				{ ! RichText.isEmpty( props.attributes.description ) && (
					<RichText.Content
						tagName="p"
						className="casino__description"
						value={ props.attributes.description }
					/> 
				) } 
			</div>
		);
	}
}
