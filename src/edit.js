import { __ } from '@wordpress/i18n';
import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';

const MY_TEMPLATE = [
	["create-block/casino-item"],
];

import './editor.scss';

export default function Edit() {
	return (
		<div {...useBlockProps()}>
            <InnerBlocks
                template={MY_TEMPLATE}
                allowedBlocks={["create-block/casino-item"]}
            />
		</div>
	);
}
